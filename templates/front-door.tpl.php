<!DOCTYPE html>
<html lang="en">
  <head>
    <?php print $scripts; ?>
    <?php print $styles; ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8" />
    <title><?php print $head_title; ?></title>
    <link rel="stylesheet" href="/welcome/css/styles.css" type="text/css" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script type="text/javascript">
      function setCookie(c_name,value,exdays) {
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        var c_path="; path=/";
        document.cookie=c_name + "=" + c_value + c_path;
      }
      function getCookie(c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
          c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
          c_value = null;
        }
        else {
          c_start = c_value.indexOf("=", c_start) + 1;
          var c_end = c_value.indexOf(";", c_start);
          if (c_end == -1) {
            c_end = c_value.length;
          }
          c_value = unescape(c_value.substring(c_start,c_end));
        }
        return c_value;
      }
      setCookie('welcomegate','passed',364);
      $welcomegate = getCookie('welcomegate');
      try{Typekit.load();}catch(e){}
      $(document).ready(function() {
        jsprettify.prettify();
      });
    </script>
  <body class="prettify">
    <div class="main">
      <div class="wrapper">
        <div class="content">
        </div>
      </div>
    </div>
    <div class="footer">
      <p>&copy; <?php echo date('Y'); ?> <?php print $site_name; ?></p>
    </div>
  </body>
</html>
